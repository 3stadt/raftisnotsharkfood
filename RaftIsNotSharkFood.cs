﻿using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class RaftIsNotSharkFood : Mod
{
    Harmony harmonyInstance;
    string harmonyId = "com.3stadt.raftisnotsharkfood";
    public void Start()
    {
        harmonyInstance = new Harmony(harmonyId);
        harmonyInstance.PatchAll(Assembly.GetExecutingAssembly());
    }

    public void OnModUnload()
    {
        harmonyInstance.UnpatchAll(harmonyId);
        Debug.Log("RaftIsNotSharkFood has been unloaded!");
    }

    [HarmonyPatch(typeof(AI_State_Attack_Block_Shark), "FindBlockToAttack")]
    public static class AIStateAttackBlockShark_FindBlockToAttack_Patch
    {
        private static void Postfix(ref Block __result)
        {
            __result = null;
        }
    }

}
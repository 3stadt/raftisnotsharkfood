# v1.2.0

- Re-download this mod *manually* in order to receive future updates
- Change of the update URL, no functional difference

# v1.1.0

- Code rewrite, no functional difference
- Overwrite result of `AI_State_Attack_Block_Shark.FindBlockToAttack` instead of setting `sharkVariables.attackRateMultiplier` to 0.

# v1.0.0

- Initial release